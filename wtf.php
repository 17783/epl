<?php
$teams = [
    "Liverpool",
    "Chelsea",
    "Tottenham Hotspur",
    "Arsenal",
    "Manchester United",
    "Everton",
    "Leicester City",
    "West Ham United",
    "Watford",
    "AFC Bournemouth",
    "Burnley",
    "Southampton",
    "Brighton & Hove Albion",
    "Norwich City",
    "Sheffield United",
    "Fulham",
    "Stoke City",
    "Middlesbrough",
    "Swansea City",
    "Derby County"
];
$tours = [];

$possibleGames = [];
$games = [];
shuffle($teams);
for ($tourId = 1; $tourId <= 19; $tourId++) {
    for ($tourMatchNo = 1; $tourMatchNo <= 10; $tourMatchNo++) {
        foreach ($teams as $team) {
            $opponents = array_values(array_diff($teams, [$team]));
            shuffle($opponents);
            foreach ($opponents as $opponent) {
                $games[] = [
                    'tour' => $tourId,
                    'tourMatchNo' => $tourMatchNo,
                    'home' => $team,
                    'away' => $opponent
                ];
            }
        }
    }
}
print_r($games);
//echo count ($teams);
?>