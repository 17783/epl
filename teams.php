<?php

class FixturesCalendar {
	public function __construct() {
		$this->fillPossibleGames();
	}
	
	/**
	 * Teams list
	 */
	public const TEAMS = [
		"Liverpool",
		"Chelsea",
		"Tottenham Hotspur",
		"Arsenal",
		"Manchester United",
		"Everton",
		"Leicester City",
		"West Ham United",
		"Watford",
		"AFC Bournemouth",
		"Burnley",
		"Southampton",
		"Brighton & Hove Albion",
		"Norwich City",
		"Sheffield United",
		"Fulham",
		"Stoke City",
		"Middlesbrough",
		"Swansea City",
		"Derby County",
	];
	/**
	 * Пул возможных игр
	 */
	private array $possibleGames = [];
	/**
	 * Игры предыдущего тура: команда -> home/away
	 */
	private array $prevTourGameTypes = [];
	
	/**
	 * Возвращает игровую пару, убирая её из пула игр
	 *
	 * @param string      $team          Команда, для которой ищем игровую пару
	 * @param array       $excludeTeams  Список команд, игры с которыми нужно исключить
	 * @param string|null $type          Тип игры (home/away/null)
	 *
	 * @return array|null
	 */
	private function getPair(string $team, array $excludeTeams, ?string $type): ?array {
		foreach ($this->possibleGames as $idx => $pair) {
			if (in_array($pair[0], $excludeTeams) || in_array($pair[1], $excludeTeams)) {
				continue;
			}
			
			if (($type === 'home' && $pair[0] === $team)
				|| ($type === 'away' && $pair[1] === $team)
				|| ($type === null && ($pair[0] === $team || $pair[1] === $team))) {
				// Убираем игру из списка возможных игр
				unset($this->possibleGames[$idx]);
				
				// Возвращаем её
				return $pair;
			}
		}
		
		// Пара не найдена
		return null;
	}
	
	private function fillPossibleGames(): void {
		$this->possibleGames = [];
		
		foreach (self::TEAMS as $team) {
			$opponents = array_values(array_diff(self::TEAMS, [$team]));
			foreach ($opponents as $opponent) {
				$key = "{$team}-{$opponent}";
				$this->possibleGames[$key] = [$team, $opponent];
			}
		}
	}
	
	public function calculate() {
		$tours = [];
		while ($this->possibleGames) {
			$tourGamePairs = $this->getTourGames();
			if (count($tourGamePairs) !== 10) {
				echo "FAIL on tour " . count($tours) . ": " . count($tourGamePairs) . " games\n";
			}
			$tours[]       = $tourGamePairs;
		}
		
		return $tours;
	}
	
	public function getTourGames(): array {
		/**
		 * Игры тура: ключ - команда, значение - тип игры
		 * Служит для понимания, что мы уже обработали эту команду в данном туре
		 */
		$tourGameTypes = [];
		
		/**
		 * Пары игр тура
		 */
		$tourGamePairs = [];
		
		foreach (self::TEAMS as $team) {
			$opponentTeam = null;
			$gameType = null;
			$opponentGameType = null;
			
			// Ищем игру для команды:
			// Если в играх тура команда уже есть - пропускаем
			if (isset($tourGameTypes[$team])) {
				continue;
			}
			
			$pair = $this->findNextPairForTeam($team, array_keys($tourGameTypes));
			
			if ($pair === null) {
				// Игры кончились
				continue;
			}
			
			$opponentTeam     = (($pair[0] === $team)) ? $pair[1] : $pair[0];
			$gameType         = ($pair[0] === $team) ? 'home' : 'away';
			$opponentGameType = $gameType === 'home' ? 'away' : 'home';
			
			// Записываем, что для этих команд игра в туре уже есть
			$tourGameTypes[$team]         = $gameType;
			$tourGameTypes[$opponentTeam] = $opponentGameType;
			
			// Записываем игровую пару
			$pairKey = "{$pair[0]}-{$pair[1]}";
			$tourGamePairs[$pairKey] = $pair;
		}
		
		$this->prevTourGameTypes = $tourGameTypes;
		
		return $tourGamePairs;
	}
	
	private function findNextPairForTeam(string $team, array $excludeTeams): ?array {
		// Если предыдущей игры не было (первый тур) - ищем любую пару
		$nextGameType = null;
		if (!isset($this->prevTourGameTypes[$team])) {
			$nextGameType = null;
		}
		// Если в предыдущем туре команда играла в гостях - ищем домашнюю пару среди тех команд, что в прошлом туре играли дома
		else if ($this->prevTourGameTypes[$team] === 'away') {
			$nextGameType = 'home';
		}
		// Если в предыдущем туре команда играла дома - ищем гостевую пару среди тех команд, что в прошлом туре играли в гостях
		else if ($this->prevTourGameTypes[$team] === 'home') {
			$nextGameType = 'away';
		}
		
		$pair = $this->getPair($team, $excludeTeams, $nextGameType);
		// Если пары не нашлось - ищем любую пару (конец выборки, исключения)
		if (!$pair) {
			$pair = $this->getPair($team, $excludeTeams, null);
		}
		
		return $pair;
	}
}

$t = new FixturesCalendar;
$tours = $t->calculate();

print_r($tours);