<?php
$teams = [
    "Liverpool",
    "Chelsea",
    "Tottenham Hotspur",
    "Arsenal",
    "Manchester United",
    "Everton",
    "Leicester City",
    "West Ham United",
    "Watford",
    "AFC Bournemouth",
    "Burnley",
    "Southampton",
    "Brighton & Hove Albion",
    "Norwich City",
    "Sheffield United",
    "Fulham",
    "Stoke City",
    "Middlesbrough",
    "Swansea City",
    "Derby County"
];
$possibleGames = []; // possible game pairs
$games = [];
shuffle($teams);
foreach ($teams as $team) {
    $opponents = array_values(array_diff($teams, [$team]));
    foreach ($opponents as $opponent) {
        $possibleGames[] = [$team, $opponent];
    }
    $opponents = [];
}

for ($tourId = 1; $tourId <= 19; $tourId++) {
    shuffle($possibleGames);
    for ($gameTourNo = 1; $gameTourNo <= 10; $gameTourNo++) {
        shuffle($possibleGames);
        $rnd = rand(0, (count($possibleGames) - 1));
        $homeGame = $possibleGames[$rnd];
        $awayGame = [$possibleGames[$rnd][1], $possibleGames[$rnd][0]];

        $games[$tourId][] = $possibleGames[$rnd];
        $awayGameId = array_search($awayGame, $possibleGames);
        $games[$tourId + 19][] = $awayGame;
        $playedHomeThisTour[] = $possibleGames[$rnd][1];
        $playedAwayThisTour[] = $possibleGames[$rnd][0];
        unset($possibleGames[$rnd]);
        unset($possibleGames[$awayGameId]);
        $possibleGames = array_values($possibleGames);
    }
}
ksort($games);
?>
<!DOCTYPE html>
<html lang="">

<head>
    <script src="https://code.jquery.com/jquery-3.6.4.js"></script>
    <title>EPL Fixtures</title>
    <style>
        body {
            font-family: Roboto, sans-serif;
            font-size: 14px;
        }

        .container {
            width: 1052px;
            margin: 0 auto;
        }

        table {
            width: 100%;
        }

        tr.game {
            background-color: #f9f9f7;
            margin-bottom: 3px;
            /*border-bottom: 1px solid #d8d8d8;*/
        }

        tr.matchday td {
            border-bottom: 2px solid #7f7f7f;
            color: #00aa32;
        }

        tr.game td {
            line-height: 24px;
            border-radius: 4px;

            /*background: rgb(242, 243, 245);*/
            border: 1px solid #e4e7ed;
        }

        .highlight {
            background-color: #b3e19d !important;
        }
    </style>
</head>

<body>
<div class="container">
    <h2>EPL Fixtures</h2>
    <table>
        <thead>
        <tr>
            <th>Home</th>
            <th>Away</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($games as $tour => $matches) : ?>
            <tr class="matchday">
                <td colspan="2">
                    <b>Matchday <?= $tour ?> of <?= count($games) ?></b>
                </td>
            </tr>
            <?php foreach ($matches as $match) : ?>
                <tr class="game team-<?= array_search($match[0], $teams) ?> team-<?= array_search($match[1], $teams) ?>">
                    <td data-team-id="team-<?= array_search($match[0], $teams) ?>">
                        <?= $match[0] ?>
                    </td>
                    <td data-team-id="team-<?= array_search($match[1], $teams) ?>">
                        <?= $match[1] ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>

<script>
    $("tr.game > td").click(function () {
        $('.highlight').removeClass('highlight')
        let teamId = $(this).data('team-id');
        $("tr." + teamId).addClass('highlight');
    });
</script>
</body>

</html>