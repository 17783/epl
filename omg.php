<?php
$teams = [
    "Liverpool",
    "Chelsea",
    "Tottenham Hotspur",
    "Arsenal",
    "Manchester United",
    "Everton",
    "Leicester City",
    "West Ham United",
    "Watford",
    "AFC Bournemouth",
    "Burnley",
    "Southampton",
    "Brighton & Hove Albion",
    "Norwich City",
    "Sheffield United",
    "Fulham",
    "Stoke City",
    "Middlesbrough",
    "Swansea City",
    "Derby County"
];
$tours = [];

$possibleGames = [];
$games = [];
for ($i = 0; $i < 19; $i++) {
    $randomizedTeams = $teams;
    shuffle($randomizedTeams);
    for ($j = 0; $j < 10; $j++) {
        $games[] = [
            $randomizedTeams[$j],
            $randomizedTeams[($j+10)]
        ];
    }
}
print_r($games);
//echo count ($teams);