<?php
$teams = [
    "Liverpool",
    "Chelsea",
    "Tottenham Hotspur",
    "Arsenal",
    "Manchester United",
    "Everton",
    "Leicester City",
    "West Ham United",
    "Watford",
    "AFC Bournemouth",
    "Burnley",
    "Southampton",
    "Brighton & Hove Albion",
    "Norwich City",
    "Sheffield United",
    "Fulham",
    "Stoke City",
    "Middlesbrough",
    "Swansea City",
    "Derby County"
];
$possibleGames = []; // possible game pairs
$games = [];
shuffle($teams);
foreach ($teams as $team) {
    $opponents = array_values(array_diff($teams, [$team]));
    foreach ($opponents as $opponent) {
        $possibleGames[] = [$team, $opponent];
    }
    $opponents = [];
}

for ($tourId = 1; $tourId <= 19; $tourId++) {
    shuffle($possibleGames);
    for ($gameTourNo = 1; $gameTourNo <= 10; $gameTourNo++) {
        shuffle($possibleGames);
        $rnd = rand(0, (count($possibleGames) - 1));
        $homeGame = $possibleGames[$rnd];
        $awayGame = [$possibleGames[$rnd][1], $possibleGames[$rnd][0]];
        $games[$tourId][] = $possibleGames[$rnd];
        $awayGameId = array_search($awayGame, $possibleGames);
        $games[$tourId + 19][] = $awayGame;
        unset($possibleGames[$rnd]);
        unset($possibleGames[$awayGameId]);
        $possibleGames = array_values($possibleGames);
    }
}
ksort($games);
//print_r($games);
foreach ($games as $tour=>$tourGames) {
    echo("\nMatchday " . $tour . " of " . count($games) . "\n\n");
    foreach ($tourGames as $tourGame) {
        echo($tourGame[0] . " => " . $tourGame[1] . "\n");
    }
}